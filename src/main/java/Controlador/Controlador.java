package Controlador;


import Modelo.Docente;
import Vista.dlgDocente;
import java.awt.HeadlessException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.management.StringValueExp;


/**
 *
 * @author Angel
 */
public class Controlador implements ActionListener{
   
    private Vista.dlgDocente vista;
    private Modelo.Docente docente;
    
    public Controlador(dlgDocente vista, Docente docente) {
        this.vista = vista;
        this.docente = docente;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        
        vista.cbNivel.addActionListener(this);
    }
    
    private void iniciarVista() {
        vista.setTitle(":: Docente :");
        vista.setSize(500, 600);
        vista.setVisible(true);
    }
    

    @Override
    public void actionPerformed(ActionEvent e) {
       if(e.getSource() == vista.btnNuevo){
            vista.btnCancelar.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.btnCerrar.setEnabled(true);            
            
            vista.txtNumDoc.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtDomicilio.setEnabled(true);
            vista.txtPagoXClase.setEnabled(true);
            vista.txtHrsTrab.setEnabled(true);
            
            vista.spHijos.setEnabled(true);
            vista.cbNivel.setEnabled(true);
            
       }else if(e.getSource()==vista.btnCerrar){
            int option = JOptionPane.showConfirmDialog(vista, "¿Deseas salir?", "Decide",JOptionPane.YES_NO_OPTION);
            
            if(option == JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
            
        }else if(e.getSource() == vista.btnGuardar){
           vista.btnGuardar.setEnabled(false);
            
           vista.txtNumDoc.setEnabled(false);
           vista.txtNombre.setEnabled(false);
           vista.txtDomicilio.setEnabled(false);
           vista.txtPagoXClase.setEnabled(false);
           vista.txtPagoXHrsTrab.setEditable(false);
           
           vista.spHijos.setEnabled(false);
           vista.cbNivel.setEnabled(false);
           
           //Envió los datos a la calse Docente
           try{
               docente.setNumDoce(Integer.parseInt(vista.txtNumDoc.getText()));
           docente.setNombre(vista.txtNombre.getText());
           docente.setDomicilio(vista.txtDomicilio.getText());
            
           System.out.println(vista.cbNivel.getSelectedIndex());

            if(vista.cbNivel.getSelectedIndex() == 0){
                 docente.setNivel(1);
            }else if(vista.cbNivel.getSelectedIndex() == 1){
                 docente.setNivel(2);
            }else if(vista.cbNivel.getSelectedIndex() == 2){
                docente.setNivel(3);
            }
           
           docente.setPagoBase(Float.parseFloat(vista.txtPagoXClase.getText()));
           docente.setHrsTrab(Float.parseFloat(vista.txtHrsTrab.getText()));
           docente.setCantidadHijos((int) vista.spHijos.getValue());
           limpiar();
           
           vista.spHijos.setValue(0);
           }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex.getMessage());
            } catch (Exception ex2){
                 JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex2.getMessage());
             } 
           
       }else if(e.getSource()==vista.btnCancelar){         
           
           vista.btnNuevo.setEnabled(true);
           
            vista.btnCancelar.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            vista.btnCerrar.setEnabled(false);            
            
            vista.txtNumDoc.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.txtDomicilio.setEnabled(false);
            vista.txtPagoXClase.setEnabled(false);
            vista.txtPagoXHrsTrab.setEditable(false);
            
            vista.spHijos.setEnabled(false);
            vista.cbNivel.setEnabled(false);
            
            vista.spHijos.setValue(0);
            vista.cbNivel.setSelectedIndex(0);
            
            limpiar();
       }else if(e.getSource()==vista.btnCerrar){
            int option = JOptionPane.showConfirmDialog(vista, "¿Deseas salir?", "Decide",JOptionPane.YES_NO_OPTION);
            
            if(option == JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
            
        }else if(e.getSource() == vista.btnMostrar){
            try{
               vista.txtNumDoc.setText(String.valueOf(docente.getNumDoce()));
                vista.txtNombre.setText(String.valueOf(docente.getNombre()));
                vista.txtDomicilio.setText(String.valueOf(docente.getDomicilio()));
                vista.txtPagoXClase.setText(String.valueOf(docente.getPagoBase()));
                vista.txtHrsTrab.setText(String.valueOf(docente.getHrsTrab()));

                vista.txtPagoXHrsTrab.setText(String.valueOf(docente.calcularPago()));
                vista.txtPagoXbono.setText(String.valueOf(docente.calcularBono((int) vista.spHijos.getValue())));
                vista.txtDescuento.setText(String.valueOf(docente.calcularImpuesto()));
                vista.txtPagoFinal.setText(String.valueOf(docente.calcularPagoFinal()));

                vista.spHijos.setValue(docente.getCantidadHijos()); 
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex.getMessage());
            } catch (Exception ex2){
                 JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: "+ex2.getMessage());
             }  
        }else if(e.getSource() == vista.btnLimpiar){
            limpiar();
        }
    }
    
    public void limpiar(){
           vista.txtNumDoc.setText("");
           vista.txtNombre.setText("");
           vista.txtDomicilio.setText("");
           vista.txtPagoXClase.setText("");
           vista.txtHrsTrab.setText("");
           
           vista.txtPagoXHrsTrab.setText("");
           vista.txtPagoXbono.setText("");
           vista.txtDescuento.setText("");
           vista.txtPagoFinal.setText("");
           
           vista.spHijos.setValue(0);
           vista.cbNivel.setSelectedIndex(0);
    }
    
    
    public static void main(String[] args) {
        dlgDocente vista = new dlgDocente(new JFrame(), true);
        Docente docente = new Docente();
        
        Controlador controlador = new Controlador(vista, docente);
       
         controlador.iniciarVista();
    }
}
