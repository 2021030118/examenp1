package Modelo;

/**
 *
 * @author Angel
 */
public class Docente {
    private int  numDocente; 
    private String nombre;
    private String domicilio;
    private int nivel;
    private float pagoBase;
    private float hrsTrab;
    private int cantidadHijos;
    
    private float pago;
    private float bono;
    private float descuento;
    
    
    
    public Docente(){}
    public Docente(Docente docente){
        this.numDocente = docente.numDocente;
        this.nombre = docente.nombre;
        this.domicilio = docente.domicilio;
        this.nivel = docente.nivel;
        this.pagoBase = docente.pagoBase;
        this.hrsTrab = docente.hrsTrab;
        this.cantidadHijos = docente.cantidadHijos;
    }
    public Docente(int numDocente, String nombre, String domicilio, int nivel, float pagoBase, float hrsTrab, int cantidad){}

    public void setNumDoce(int numDocente){
        this.numDocente = numDocente;
    }
    
    public int getNumDoce(){
        return numDocente;
    }
    
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    
    public String getNombre(){
        return nombre;
    }
    
     public void setDomicilio(String domicilio){
        this.domicilio = domicilio;
    }
    
    public String getDomicilio(){
        return domicilio;
    }
    
    public void setNivel(int nivel){
        this.nivel = nivel;
    }
    
    public int getNivel(){
        return nivel;
    }
    
     public void setPagoBase(float pagoBase){
        this.pagoBase = pagoBase;
    }
    
    public float getPagoBase(){
        return pagoBase;
    }
    
     public void setHrsTrab(float hrsTrab){
        this.hrsTrab = hrsTrab;
    }
    
    public float getHrsTrab(){
        return hrsTrab;
    }
    
     public void setCantidadHijos(int cantidadHijos){
        this.cantidadHijos = cantidadHijos;
    }
    
    public int getCantidadHijos(){
        return cantidadHijos;
    }
    
    public float calcularPago(){
        
        System.out.println("Nivel"+nivel);
        
        
        if(nivel == 1)
           pago = (float) ((pagoBase * hrsTrab) + ((pagoBase * hrsTrab) * (0.30)));
        else if(nivel == 2)
           pago =(float) ((pagoBase * hrsTrab) + ((pagoBase * hrsTrab) * (0.50)));
        else if(nivel == 3)
            pago = (float) ((pagoBase * hrsTrab) + ((pagoBase * hrsTrab) * (1)));
        
        return pago;
    }
    
    public float calcularImpuesto(){
        
        descuento = pago * 0.16f;
        
        return descuento;
    }
    
    public float calcularBono(int cantidad){
        if(cantidadHijos >=1 && cantidadHijos <=2){
           bono = (float)  (pago * 0.05f);
            System.out.println("bono: "+bono);
        }else if(cantidadHijos >= 3 && cantidadHijos <=5){
              bono = (float) (pago * 0.10f);
            System.out.println("bono: "+bono);
         }else if(cantidadHijos >= 3 && cantidadHijos <=5){
             bono = (float)  (pago * 0.20f);
         }
        
        return bono;
    } 
    
    public float calcularPagoFinal(){
        return (pago+bono) - descuento;
    }
}
